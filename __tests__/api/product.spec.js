const frisby = require('frisby');
const settings = require('../../settings');
const { generatePostBody } = require('../api/templates/create_product_template');
const { generatePutBody } = require('../api/templates/modify_product_template');

const { Joi } = frisby;
const PRODUCT_ENDPOINT = `${settings.HOST}/product`;


describe('POST product/ - create a product', () => {
  it('should create a new product', (done) => {
    const POST_BODY = generatePostBody();
    // When
    frisby.post(PRODUCT_ENDPOINT, POST_BODY)

    // Then
      .expect('status', 200)
      .expect('json', 'name', POST_BODY.name)
      .expect('json', 'description', POST_BODY.description)
      .expect('json', 'price', POST_BODY.price)
      .expect('jsonTypes', '_id', Joi.string().hex())
      .done(done);
  });
});

describe('PUT product/:id - modify a product', () => {
  it('should be able to modify a product', (done) => {
    const POST_BODY = generatePostBody();
    // Given
    frisby.post(PRODUCT_ENDPOINT, POST_BODY)
      .expect('status', 200)
      .expect('json', 'name', POST_BODY.name)
      .expect('json', 'description', POST_BODY.description)
      .expect('json', 'price', POST_BODY.price)
      .expect('jsonTypes', '_id', Joi.string().hex())
      .then((res) => {
        const id = res.json._id;
        const PUT_PRODUCT_ENDPOINT = `${PRODUCT_ENDPOINT}/${id}`;
        const PUT_BODY = generatePutBody();
        // When
        frisby.put(PUT_PRODUCT_ENDPOINT, PUT_BODY)

          // Then
          .expect('status', 200)
          .expect('json', 'name', POST_BODY.name)
          .expect('json', 'description', POST_BODY.description)
          .expect('json', 'price', PUT_BODY.price)
          .expect('jsonTypes', '_id', Joi.string().hex())
          .expect('jsonTypes', 'id', Joi.string().hex())
          .expect('jsonTypes', 'createdAt', Joi.string().isoDate())
          .expect('jsonTypes', 'updatedAt', Joi.string().isoDate())
          .expect('jsonTypes', '__v', Joi.number().min(0))
          .done(done);
      });
  });
});

describe('DELETE product/:id - delete a product from database', () => {
  it('should be able to properly delete an already existing item', (done) => {
    const POST_BODY = generatePostBody();
    // Given
    frisby.post(PRODUCT_ENDPOINT, POST_BODY)
      .expect('status', 200)
      .expect('json', 'name', POST_BODY.name)
      .expect('json', 'description', POST_BODY.description)
      .expect('json', 'price', POST_BODY.price)
      .expect('jsonTypes', '_id', Joi.string().hex())
      .then((res) => {
        const id = res.json._id;
        const DELETE_PRODUCT_ENDPOINT = `${PRODUCT_ENDPOINT}/${id}`;

        // When
        frisby.del(DELETE_PRODUCT_ENDPOINT)

          // Then
          .expect('status', 200)
          .expect('json', 'name', POST_BODY.name)
          .expect('json', 'description', POST_BODY.description)
          .expect('json', 'price', POST_BODY.price)
          .expect('json', '_id', id)
          .expect('json', 'id', id)
          .expect('jsonTypes', 'createdAt', Joi.string().isoDate())
          .expect('jsonTypes', 'updatedAt', Joi.string().isoDate())
          .done(done);
      });
  });
});

describe('GET product/:id - get one specific product', () => {
  it('should be able to get a specific product by id', (done) => {
    const POST_BODY = generatePostBody();
    // Given
    frisby.post(PRODUCT_ENDPOINT, POST_BODY)
      .expect('status', 200)
      .expect('json', 'name', POST_BODY.name)
      .expect('json', 'description', POST_BODY.description)
      .expect('json', 'price', POST_BODY.price)
      .expect('jsonTypes', '_id', Joi.string().hex())
      .then((res) => {
        const id = res.json._id;
        const GET_PRODUCT_ENDPOINT = `${PRODUCT_ENDPOINT}/${id}`;

        // When
        frisby.get(GET_PRODUCT_ENDPOINT)

          // Then
          .expect('status', 200)
          .expect('json', 'name', POST_BODY.name)
          .expect('json', 'description', POST_BODY.description)
          .expect('json', 'price', POST_BODY.price)
          .expect('json', '_id', id)
          .expect('json', 'id', id)
          .expect('jsonTypes', 'createdAt', Joi.string().isoDate())
          .expect('jsonTypes', 'updatedAt', Joi.string().isoDate())
          .done(done);
      });
  });
});

describe('GET product/ - get the list of products', () => {
  it('should respond with status code 200', (done) => {
    // When
    frisby.get(PRODUCT_ENDPOINT)

      // Then
      .expect('status', 200)
      .expect('jsonTypesStrict', '[*].name', Joi.string())
      .expect('jsonTypesStrict', '[*].description', Joi.string())
      .expect('jsonTypesStrict', '[*].price', Joi.number().positive())
      .expect('jsonTypesStrict', '[*]._id', Joi.string().hex())
      .expect('jsonTypesStrict', '[*].id', Joi.string().hex())
      .expect('jsonTypesStrict', '[*].createdAt', Joi.string().isoDate())
      .expect('jsonTypesStrict', '[*].updatedAt', Joi.string().isoDate())
      .expect('jsonTypesStrict', '[*].__v', Joi.number().min(0))
      .done(done);
  });
});
