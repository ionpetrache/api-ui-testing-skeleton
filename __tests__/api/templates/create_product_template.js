const faker = require('faker');

module.exports = {
  generatePostBody: () => ({
    name: faker.commerce.productName(),
    description: faker.commerce.productAdjective(),
    price: parseFloat(faker.commerce.price()),
  }),
};
