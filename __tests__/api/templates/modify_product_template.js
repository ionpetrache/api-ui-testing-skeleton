const faker = require('faker');

module.exports = {
  generatePutBody: () => ({ price: parseFloat(faker.commerce.price()) }),
};
