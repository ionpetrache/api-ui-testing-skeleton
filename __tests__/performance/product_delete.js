import http from 'k6/http';
import { group, check, sleep } from 'k6';

export const options = {
  stages: [
    { duration: '2s', target: 10 },
    { duration: '5s', target: 20 },
    { duration: '2s', target: 0 },
  ],
};

const endpoint = 'http://localhost:1337/product';

function randomNumber(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

export default function () {
  group('create a product and then delete it', () => {
    const id = randomNumber(1, 10000);
    const name = `Product ${id}`;
    const price = randomNumber(1, 1000);
    const description = 'Load testing for Product';
    const payload = JSON.stringify({ name, description, price });
    const params = { headers: { 'Content-Type': 'application/json' } };

    const postRes = http.post(endpoint, payload, params);

    check(postRes, {
      'POST status was 200': r => r.status === 200,
      'Body has correct description': r => JSON.parse(r.body).description === description,
    });


    const productId = JSON.parse(postRes.body).id;
    const deleteEndpoint = `${endpoint}/${productId}`;
    const deleteRes = http.del(deleteEndpoint);
    check(deleteRes, {
      'DELETE status was 200': r => r.status === 200,
    });

    sleep(1);
  });
}
