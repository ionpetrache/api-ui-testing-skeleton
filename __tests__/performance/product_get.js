import http from 'k6/http';
import { group, check, sleep } from 'k6';

export const options = {
  stages: [
    { duration: '15s', target: 100 },
    { duration: '30s', target: 200 },
    { duration: '15s', target: 0 },
  ],
};

export default function () {
  group('Get entire list of products', () => {
    const res = http.get('http://localhost:1337/product');
    check(res, {
      'status was 200': r => r.status === 200,
      'transaction time OK': r => r.timings.duration < 200,
    });
    sleep(1);
  });
}
