import http from 'k6/http';
import { group, check, sleep } from 'k6';

export const options = {
  stages: [
    { duration: '15s', target: 10 },
    { duration: '30s', target: 20 },
    { duration: '15s', target: 0 },
  ],
};

export default function () {
  group('Modify an already existing product', () => {
    const endpoint = 'http://localhost:1337/product/5b98ffacbb8245390d02176a';
    const payload = JSON.stringify({ description: 'load test creating Product X - modify' });
    const params = { headers: { 'Content-Type': 'application/json' } };
    const res = http.put(endpoint, payload, params);
    check(res, {
      'status was 200': r => r.status === 200,
    });
    sleep(1);
  });
}
