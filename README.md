# API and UI tests - skeleton project

## How to set up your development machine and use the project

1.  Install NodeJS on your machine from the website [Node](https://nodejs.org/en/) or using the terminal - MAC OS X:</br>
    ```
    brew install node
    brew tap loadimpact/k6
    brew install k6
    ```

2.  Clone the repository into your machine:</br>
    `git clone <report>`

3.  If you run on Linux it might be possible that you need to install also some libraries in order to let chrome run.<br/>The list of dependencies you can find here [Puppeteer](https://github.com/GoogleChrome/puppeteer/blob/master/docs/troubleshooting.md):

4.  Install all the project dependencies:
    `npm install`

5.  The project is using .dotenv to load environment variables.</br>
    Make sure you create environment variables like in [.env.sample](./.env.sample) or inject them in your environment.</br>
    On GitLab the some of the environment variables have been defined at project level. In order to change them you need to go to: Settings -> CI / CD -> Variables and click expand to see them and modify.

6.  Write your API tests in [api](./__tests__/api) folder.
    Test files should follow the naming convention: `*.spec.js`

7.  Write your API requests in [templates](./__tests__/api/templates) folder.
    Afterwards you can import the   template and use it in your test files.

8.  Write the UI tests in the [api](./__tests__/ui) folder.
    Test files should follow the naming convention: `*.spec.js`

9.  Write your UI page objects in [pages](./__tests__/ui/pages) folder.
    Naming convention for page objects is: `*.po.js`

10.  Run the API - tests:
    `npm run test:api`

11.  Run the UI - tests:
    `npm run test:ui`

12.  After running any of the tests API or UI you can check the results in console or in the test report that is generated inside reports folder

## Performance testing with K6
* Some small example script are in [performance](./__tests__/performance) folder.
* You can run the scripts with: `k6 run script.js`
* You can record a browser session as a HAR file, convert is as a JS script and run it with k6. Example at the [link](https://docs.k6.io/docs/session-recording-har-support)
* More samples on how to write the scripts on the [k6](https://github.com/loadimpact/k6/tree/master/samples)

## Other dependencies

The project is using:

- [Frisbyjs](https://www.frisbyjs.com/) for the API tests
- [Puppeteer](https://pptr.dev/) for the UI.
- The testing framework used is [Jest](http://jestjs.io/)
- JSON response validation using [Joi](https://github.com/hapijs/joi)
- For load / performance testing [k6](https://k6.io/)

Have fun testing :rocket:
