require('dotenv').config();

const settings = {
  HOST: process.env.HOST,
  USER_EMAIL: process.env.USER_EMAIL,
  USER_PASS: process.env.USER_PASS,
  API_VERSION: process.env.API_VERSION,
  API_KEY: process.env.API_KEY,
  STORE_ENDPOINT: `${process.env.TM_HOST}${process.env.TM_API_VERSION}/content/store`,
  DELETE_ENDPOINT: `${process.env.TM_HOST}${process.env.TM_API_VERSION}/content/delete`,
  timeouts: {
    XXS: 500,
    XS: 1000,
    S: 5000,
    M: 10000,
    L: 30000,
    XL: 60000,
    XXL: 120000,
  },
  resolutions: {
    desktopSmall: {
      width: 1280,
      height: 631,
    },
    desktopMedium: {
      width: 1920,
      height: 1080,
    },
    desktopLarge: {
      width: 2560,
      height: 1440,
    },
    phone: {
      width: 414,
      height: 736,
    },
    tablet: {
      width: 768,
      height: 1024,
    },
  },
  launchConfig: {
    headMode: {
      dumpio: true,
      headless: false,
      slowMo: 100,
      args: ['--disable-dev-shm-usage', '--start-fullscreen'],
    },

    headlessMode: {
      dumpio: true,
      headless: true,
      args: ['--disabled-dev-shm-usage', '--no-sandbox'],
    },
  },

  getViewport(mode) {
    if (process.env.DEBUG_TESTS) {
      return this.resolutions.desktopLarge;
    }

    if (mode === 'S') {
      return this.resolutions.desktopSmall;
    } else if (mode === 'M') {
      return this.resolutions.desktopMedium;
    } else if (mode === 'L') {
      return this.resolutions.desktopLarge;
    }
    return this.resolutions.desktopSmall;
  },
  getLaunchConfig() {
    if (process.env.DEBUG_TESTS) {
      return this.launchConfig.headMode;
    }
    return this.launchConfig.headlessMode;
  },
};

module.exports = settings;
