module.exports = {
  verbose: true,
  bail: true,
  reporters: [
    'default', ['./node_modules/jest-html-reporter', {
      pageTitle: 'Traffic Module - Test Report',
      outputPath: './reports/test-report.html',
      includeFailureMsg: true,
      includeConsoleLog: true,
    }],
  ],
};
